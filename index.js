const express = require("express");
const { OAuth } = require("oauth");
const fs = require("fs");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const morgan = require("morgan");
const FileStore = require("session-file-store")(session);

var app = (module.exports = express());

app.use(morgan("tiny"));
app.use(cookieParser());
app.use(
  session({
    secret: "secret",
    resave: false,
    saveUninitialized: false,
    store: new FileStore({}),
  })
);

var config = require("./config.json");
const { exception } = require("console");

var privateKeyData = fs.readFileSync(config["consumerPrivateKeyFile"], "utf8");

var consumer = new OAuth(
  "https://perflo.atlassian.net/plugins/servlet/oauth/request-token",
  "https://perflo.atlassian.net/plugins/servlet/oauth/access-token",
  config["consumerKey"],
  "",
  "1.0",
  "http://localhost:5000/sessions/callback",
  "RSA-SHA1",
  null,
  privateKeyData
);

app.get("/", function (request, response) {
  response.send("Hello world!");
});

app.get("/sessions/connect", function (request, response) {
  consumer.getOAuthRequestToken(function (
    error,
    oauthToken,
    oauthTokenSecret,
    results
  ) {
    if (error) {
      console.log(error);
      response.send("Error getting OAuth access token");
    } else {
      request.session.oauthRequestToken = oauthToken;
      request.session.oauthRequestTokenSecret = oauthTokenSecret;
      request.session.next = request.query.next;
      response.redirect(
        `https://perflo.atlassian.net/plugins/servlet/oauth/authorize?oauth_token=${request.session.oauthRequestToken}`
      );
    }
  });
});

app.get("/sessions/callback", function (request, response) {
  consumer.getOAuthAccessToken(
    request.session.oauthRequestToken,
    request.session.oauthRequestTokenSecret,
    request.query.oauth_verifier,
    function (error, oauthAccessToken, oauthAccessTokenSecret, results) {
      if (error) {
        console.log(error.data);
        response.send("error getting access token");
      } else {
        request.session.oauthAccessToken = oauthAccessToken;
        request.session.oauthAccessTokenSecret = oauthAccessTokenSecret;
        if (request.session.next) {
          response.redirect(request.session.next);
        } else {
          response.redirect("/");
        }
      }
    }
  );
});

app.get("/boards/", function (request, response, next) {
  consumer.get(
    "https://perflo.atlassian.net/rest/agile/1.0/board",
    request.session.oauthAccessToken,
    request.session.oauthAccessTokenSecret,
    "application/json",
    function (error, data, resp) {
      if (error) {
        next(error);
      } else {
        response.set("Content-Type", "application/json");
        response.send(data);
      }
    }
  );
});

app.get("/:boardId(\\d+)/epics/", function (request, response, next) {
  consumer.get(
    `https://perflo.atlassian.net/rest/agile/1.0/board/${request.params.boardId}/epic`,
    request.session.oauthAccessToken,
    request.session.oauthAccessTokenSecret,
    "application/json",
    function (error, data, resp) {
      if (error) {
        next(error);
      } else {
        response.set("Content-Type", "application/json");
        response.send(data);
      }
    }
  );
});

app.listen(parseInt(process.env.PORT || 5000));
