# Jira Project Getter

## Introduction

Gets Jira boards and epics using OAuth 1.0a and Jira Rest API. POC web app.

## Install and Run

```sh
npm install
npm run dev  # start dev server
```

## Workflow

Navigate to http://localhost:5000/sessions/connect to start Oauth process.

Navigate to http://localhost:5000/boards/ to get boards.

Navigate to http://localhost:5000/1/epics/ to get epics of a board.

## Reference

[JIRA Agile Server REST API reference](https://docs.atlassian.com/jira-software/REST/7.3.1/)

[JIRA Oauth](https://developer.atlassian.com/server/jira/platform/oauth/)

Code is modified and adapted from https://bitbucket.org/atlassianlabs/atlassian-oauth-examples/src/master/nodejs/app.js
